-- Pour chaque état, le nombre de sols en pierre 
-- et de latrines type « fermées » (WC) dans les villes
SELECT sta.stateName, flo.stone, lat.wc
FROM state_loc staloc
INNER JOIN flooring flo
	ON flo.idStateLoc = staloc.idStateLoc
INNER JOIN latrine lat
	ON lat.idStateLoc = staloc.idStateLoc
INNER JOIN state sta
	ON sta.idState = staloc.idState
INNER JOIN location loc
	ON loc.idLoc = staloc.idLoc
    AND loc.locType = 'Urban';

-- Le nombre de sols en mosaïque en ville 
-- pour l'état « UNION TERRITORY - PONDICHERRY  34 »
SELECT sta.stateName, flo.mosaic
FROM state_loc staloc
INNER JOIN flooring flo
	ON flo.idStateLoc = staloc.idStateLoc
INNER JOIN state sta
	ON sta.idState = staloc.idState
INNER JOIN location loc
	ON loc.idLoc = staloc.idLoc
    AND loc.locType = 'Urban'
WHERE sta.stateName = 'UNION TERRITORY - PONDICHERRY  34';

-- Le nombre de sols en mosaïque total (ville et campagne) 
-- pour l'état de « UNION TERRITORY - PONDICHERRY  34 »
SELECT sta.stateName, SUM(flo.mosaic)
FROM state_loc staloc
INNER JOIN flooring flo
	ON flo.idStateLoc = staloc.idStateLoc
INNER JOIN state sta
	ON sta.idState = staloc.idState
INNER JOIN location loc
	ON loc.idLoc = staloc.idLoc
WHERE sta.stateName = 'UNION TERRITORY - PONDICHERRY  34'
GROUP BY sta.stateName;

-- Pour chaque état le nombre de sols en pierre 
-- et de latrines type «fermées» (WC) dans les villes 
-- dont le nombre de sols en pierre est supérieur à 50000
SELECT sta.stateName, flo.stone, lat.wc
FROM state_loc staloc
INNER JOIN flooring flo
	ON flo.idStateLoc = staloc.idStateLoc
    AND flo.stone > 50000
INNER JOIN latrine lat
	ON lat.idStateLoc = staloc.idStateLoc
INNER JOIN state sta
	ON sta.idState = staloc.idState
INNER JOIN location loc
	ON loc.idLoc = staloc.idLoc
    AND loc.locType = 'Urban';

-- La liste des états classée par nombre d'habitations 
-- en campagne du plus bas au plus élevé (utilisez les sols)
SELECT sta.stateName, SUM(flo.mud + flo.wood + flo.brick + flo.stone + flo.cement + flo.mosaic + flo.otherFloor) total_of_rural_homes
FROM state_loc staloc
INNER JOIN flooring flo
	ON flo.idStateLoc = staloc.idStateLoc
INNER JOIN state sta
	ON sta.idState = staloc.idState
INNER JOIN location loc
	ON loc.idLoc = staloc.idLoc
    AND loc.locType = 'Rural'
GROUP BY sta.stateName
ORDER BY total_of_rural_homes ASC;

-- La liste des 3 états ayant le nombre 
-- d'habitations urbaines le plus élevé
SELECT sta.stateName, SUM(flo.mud + flo.wood + flo.brick + flo.stone + flo.cement + flo.mosaic + flo.otherFloor) total_of_urban_homes
FROM state_loc staloc
INNER JOIN flooring flo
	ON flo.idStateLoc = staloc.idStateLoc
INNER JOIN state sta
	ON sta.idState = staloc.idState
INNER JOIN location loc
	ON loc.idLoc = staloc.idLoc
    AND loc.locType = 'Urban'
GROUP BY sta.stateName
ORDER BY total_of_urban_homes DESC
LIMIT 3;

-- Pour chaque état l'utilisation du bois / bambou 
-- dans les habitations dans les campagnes, 
-- de la plus élevée à la plus faible en valeur absolue
SELECT sta.stateName, flo.wood
FROM state_loc staloc
INNER JOIN flooring flo
	ON flo.idStateLoc = staloc.idStateLoc
INNER JOIN state sta
	ON sta.idState = staloc.idState
INNER JOIN location loc
	ON loc.idLoc = staloc.idLoc
    AND loc.locType = 'Rural'
ORDER BY flo.wood DESC;

-- Pour chaque état l'utilisation du bois / bambou  
-- dans les campagnes, de la plus élevée à la plus faible 
-- en valeur relative au nombre total d'habitations de l'état
SELECT 
	sta.stateName, 
	flo.wood, 
	SUM(flo.mud + flo.wood + flo.brick + flo.stone + flo.cement + flo.mosaic + flo.otherFloor) total_of_houses, 
    SUM((flo.wood / (flo.mud + flo.wood + flo.brick + flo.stone + flo.cement + flo.mosaic + flo.otherFloor)) * 100) wood_ratio
FROM state_loc staloc
INNER JOIN flooring flo
	ON flo.idStateLoc = staloc.idStateLoc
INNER JOIN state sta
	ON sta.idState = staloc.idState
INNER JOIN location loc
	ON loc.idLoc = staloc.idLoc
    AND loc.locType = 'Rural'
GROUP BY sta.stateName, flo.wood
ORDER BY wood_ratio DESC;

-- La somme totale des latrines type « fermées » (WC) en Inde
SELECT SUM(lat.wc)
FROM latrine lat;