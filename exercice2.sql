-- Justification des choix de modélisation en fin de fichier

-- Création de la table "state"
CREATE TABLE state (
	idState		INT(4) UNSIGNED NOT NULL AUTO_INCREMENT,
    stateName	VARCHAR(128) NOT NULL,
    PRIMARY KEY (idState)
);

-- Création de la table "location"
CREATE TABLE location (
	idLoc		INT(4) UNSIGNED NOT NULL AUTO_INCREMENT,
    locType		VARCHAR(32) NOT NULL,
    PRIMARY KEY (idLoc)
);

-- Création de la table "state_loc"
CREATE TABLE state_loc (
    idStateLoc  INT(4) UNSIGNED NOT NULL AUTO_INCREMENT,
    idState		INT(4) NOT NULL,
    idLoc		INT(4) NOT NULL,
    PRIMARY KEY (idStateLoc),
    UNIQUE KEY (idState, idLoc)
);

-- Création de la table "latrine"
CREATE TABLE latrine (
    idLat		INT(4) UNSIGNED NOT NULL AUTO_INCREMENT,
    pitLat		INT(4) UNSIGNED DEFAULT 0,
    wc			INT(4) UNSIGNED DEFAULT 0,
    otherLat	INT(4) UNSIGNED DEFAULT 0,
    noLat		INT(4) UNSIGNED DEFAULT 0,
    idStateLoc	INT(4) UNSIGNED NOT NULL,
    PRIMARY KEY (idLat)
);

-- Création de la table "flooring"
CREATE TABLE flooring (
    idFloor		INT(4) UNSIGNED NOT NULL AUTO_INCREMENT,
    mud			INT(4) UNSIGNED DEFAULT 0,
    wood		INT(4) UNSIGNED DEFAULT 0,
    brick		INT(4) UNSIGNED DEFAULT 0,
    stone		INT(4) UNSIGNED DEFAULT 0,
    cement		INT(4) UNSIGNED DEFAULT 0,
    mosaic		INT(4) UNSIGNED DEFAULT 0,
    otherFloor	INT(4) UNSIGNED DEFAULT 0,
    idStateLoc	INT(4) UNSIGNED NOT NULL,
    PRIMARY KEY (idFloor)
);

-- Les données de type état (state) et rural/urbain (location)
-- se répètent, non seulement entre les deux documents de base,
-- mais aussi dans chacun d'eux (état1/rural ; état1/urbain, etc).

-- Créer une table distincte pour chacune de ces données m'a donc paru judicieux.
-- En outre, la liaison des deux constitue un identifiant unique permettant
-- d'identifier chaque ligne d'entrée dans les deux tables restantes (latrine et flooring).