-- CREATION ET PEUPLEMENT DE DEUX TABLES TEMPORAIRES
-- POUR ACCUEILLIR L'INFORMATION BRUTE

-- Création de la table "state_lat"
CREATE TABLE state_lat (
    idStateLat	INT(4) UNSIGNED NOT NULL AUTO_INCREMENT,
    stateName	VARCHAR(128) NOT NULL,
    rurUrb		VARCHAR(32) NOT NULL,
    pitLat		INT(4) UNSIGNED DEFAULT 0,
    wc			INT(4) UNSIGNED DEFAULT 0,
    otherLat	INT(4) UNSIGNED DEFAULT 0,
    noLat		INT(4) UNSIGNED DEFAULT 0,
    PRIMARY KEY (idStateLat)
);

-- Peuplement de la table "state_lat"
-- avec les données formatées 
-- du fichier s2_exam_sql_annexe_1_format.csv
INSERT INTO state_lat (stateName, rurUrb, pitLat, wc, otherLat, noLat)
VALUES 
('STATE - ANDHRA PRADESH  28','Rural','809705','1093148','397842','10375523'),
-- ...
('UNION TERRITORY - PONDICHERRY  34','Urban','2994','81057','4692','47713');

-- Création de la table "state_floor"
CREATE TABLE state_floor (
    idStateFloor	INT(4) UNSIGNED NOT NULL AUTO_INCREMENT,
    stateName		VARCHAR(128) NOT NULL,
    rurUrb			VARCHAR(32) NOT NULL,
    mud				INT(4) UNSIGNED DEFAULT 0,
    wood			INT(4) UNSIGNED DEFAULT 0,
    brick			INT(4) UNSIGNED DEFAULT 0,
    stone			INT(4) UNSIGNED DEFAULT 0,
    cement			INT(4) UNSIGNED DEFAULT 0,
    mosaic			INT(4) UNSIGNED DEFAULT 0,
    otherFloor		INT(4) UNSIGNED DEFAULT 0,
    PRIMARY KEY (idStateFloor)
);

-- Peuplement de la table "state_floor"
-- avec les données formatées 
-- du fichier s2_exam_sql_annexe_2_format.csv
INSERT INTO state_floor (stateName, rurUrb, mud, wood, brick, stone, cement, mosaic, otherFloor)
VALUES 
('STATE - ANDHRA PRADESH  28','Rural','7997880','45420','94488','3829081','2931084','84033','48655'),
-- ...
('UNION TERRITORY - PONDICHERRY  34','Urban','25155','203','928','428','120156','29677','680');

-- PEUPLEMENT DES TABLES

-- A PARTIR DES TABLES TEMPORAIRES
-- Table "state"
INSERT INTO state(stateName)
SELECT DISTINCT sl.stateName
FROM state_lat sl
ORDER BY sl.stateName;

-- Table "location"
INSERT INTO location(locType)
SELECT DISTINCT sl.rurUrb
FROM state_lat sl
ORDER BY sl.rurUrb;

-- A PARTIR DES TABLES DEJA GENEREES (state + location)
-- Table "state_loc"
INSERT INTO state_loc(idState, idLoc)
SELECT DISTINCT sta.idState, loc.idLoc
FROM state sta
INNER JOIN location loc 
   ON loc.idLoc
ORDER BY sta.idState ASC;

-- A PARTIR DES TABLES TEMPORAIRES ET DES TABLES DEJA GENEREES
-- Table "latrine"
INSERT INTO latrine (pitLat, wc, otherLat, noLat, idStateLoc)
SELECT stalat.pitLat, stalat.wc, stalat.otherLat, stalat.noLat, staloc.idStateLoc
FROM state_lat stalat
INNER JOIN state sta
	ON sta.stateName = stalat.stateName
INNER JOIN location loc
	ON loc.locType = stalat.rurUrb
INNER JOIN state_loc staloc
	ON staloc.idState = sta.idState
	AND staloc.idLoc = loc.idLoc
ORDER BY staloc.idStateLoc;

-- Table "flooring"
INSERT INTO flooring (mud, wood, brick, stone, cement, mosaic, otherFloor, idStateLoc)
SELECT staflo.mud, staflo.wood, staflo.brick, staflo.stone, staflo.cement, staflo.mosaic, staflo.otherFloor, idStateLoc
FROM state_floor staflo
INNER JOIN state sta
	ON sta.stateName = staflo.stateName
INNER JOIN location loc
	ON loc.locType = staflo.rurUrb
INNER JOIN state_loc staloc
	ON staloc.idState = sta.idState
	AND staloc.idLoc = loc.idLoc
ORDER BY staloc.idStateLoc;