## Identifiants de l'utilisateur de la base de données

* login : ips_admin

* password : g54Gtr0@!bbrT15;

## Instruction de création de l'utilisateur



    -- Création de l'utilisateur ips_admin
    CREATE USER 'ips_admin'@'localhost'
    IDENTIFIED BY 'g54Gtr0@!bbrT15;';

    -- Attribution des droits d'administration à ips_admin
    -- sur toutes les tables de la db indian_people_stats
    GRANT ALL PRIVILEGES
    ON indian_people_stats.*
    TO 'ips_admin'@'localhost';

    -- Vidage du cache MySQL
    -- pour que les nouveaux droits s'appliquent
    FLUSH PRIVILEGES;