 -- Connexion au serveur MySQL en tant qu'utilisateur root
 mysql -u root -p;

-- En tant que root :

-- Création de la base de données (db) indian_people_stats
CREATE DATABASE indian_people_stats
DEFAULT CHARACTER SET utf8
COLLATE utf8_general_ci;

-- Création de l'utilisateur ips_admin
CREATE USER 'ips_admin'@'localhost'
IDENTIFIED BY 'g54Gtr0@!bbrT15;';

-- Attribution des droits d'administration à ips_admin
-- sur toutes les tables de la db indian_people_stats
GRANT ALL PRIVILEGES
ON indian_people_stats.*
TO 'ips_admin'@'localhost';

-- Vidage du cache MySQL
-- pour que les nouveaux droits s'appliquent
FLUSH PRIVILEGES;

-- Connexion au serveur MySQL, sur la db 
-- en tant qu'utilisateur ips_admin indian_people_stats
mysql -u ips_admin -p;

-- En tant que ips_admin :

-- Positionnement sur la db indian_people_stats;
USE indian_people_stats;